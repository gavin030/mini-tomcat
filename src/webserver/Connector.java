package webserver;

import webserver.threadpool.ThreadPool;
import webserver.http.Processor;

import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.io.IOException;
import java.net.InetAddress;

import org.apache.log4j.Logger;

import com.sun.corba.se.impl.protocol.giopmsgheaders.Message;



public class Connector {
	
	private final int port;
	private final String root;
	private final ThreadPool threadPool;
	private Logger logger;
	private int count = 0;
	
	public Connector(int port, String root){
		this.port = port;
		this.root = root;
		threadPool = new ThreadPool(60);
		threadPool.setLogger(logger);
	}
	
	public void listen(){
		try(ServerSocket serverSocket = new ServerSocket(port, 1, InetAddress.getByName("127.0.0.1"))){
			while(!threadPool.isStopped()){
				Socket socket = null;
				try{
					//Listens for a connection to be made to this socket and accepts it. The method blocks until a connection is made.
					socket = serverSocket.accept();
				}catch(IOException ex){
					ex.printStackTrace();
				}finally{
					if(socket != null){
						socket.setSoTimeout(10000);
						count++;
						logger.info("got " + count +" sockets");
						threadPool.addTask(new Processor(socket, root));
						/*single thread mode
						Processor test = new Processor(socket, root);
						test.run();
						*/
					}
				}
			}
		}catch(UnknownHostException ex){
			logger.error("", ex);
		}catch(IOException ex){
			logger.error("", ex);
		}
		logger.info("end of the program");
		return;
	}
	
	public void setLogger(Logger logger){
		this.logger = logger;
	}
}


