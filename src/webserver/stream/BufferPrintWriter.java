package webserver.stream;

import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.Writer;

public class BufferPrintWriter extends PrintWriter{
	private int bufferSize;
	private StringBuffer buffer;
	
	public BufferPrintWriter(OutputStream output, int bufferSize){
		super(output);
		this.bufferSize = bufferSize / 2;//covert byte length to char length
		buffer = new StringBuffer(this.bufferSize);
	}
	
	public BufferPrintWriter(Writer writer, int bufferSize){
		super(writer);
		this.bufferSize = bufferSize;
		buffer = new StringBuffer(this.bufferSize);
	}
	
	@Override
	public void print(String s){
		String addition = s;
		if(addition.length() + buffer.length() > bufferSize) flush();
		buffer.append(addition);
	}
	
	@Override
	public void print(boolean b){
		print(String.valueOf(b));
	}
	
	@Override
	public void print(char c){
		print(String.valueOf(c));
	}
	
	@Override
	public void print(char[] s){
		print(String.valueOf(s));
	}
	
	@Override
	public void print(double d){
		print(String.valueOf(d));
	}
	
	@Override
	public void print(float f){
		print(String.valueOf(f));
	}
	
	@Override
	public void print(int i){
		print(String.valueOf(i));
	}
	
	@Override
	public void print(long l){
		print(String.valueOf(l));
	}
	
	@Override
	public void print(Object obj){
		print(String.valueOf(obj));
	}
	
	@Override
	public void println(){
		print("\n");
	}
	
	@Override
	public void println(boolean x){
		print(x + "\n");
	}
	
	@Override
	public void println(char x){
		print(x + "\n");
	}
	
	@Override
	public void println(char[] x){
		print(String.valueOf(x) + "\n");
	}
	
	@Override
	public void println(double x){
		print(x + "\n");
	}
	
	@Override
	public void println(float x){
		print(x + "\n");
	}
	
	@Override
	public void println(int x){
		print(x + "\n");
	}
	
	@Override
	public void println(long x){
		print(x + "\n");
	}
	
	@Override
	public void println(Object x){
		print(x + "\n");
	}
	
	@Override
	public void println(String x){
		print(x + "\n");
	}
	
	@Override
	public BufferPrintWriter printf(String format, Object... args){
		print(String.format(format, args));
		return this;
	}
	
	@Override
	public void flush(){
		char[] bufferArray = new char[buffer.length()];
		buffer.getChars(0, buffer.length(), bufferArray, 0);
		super.write(bufferArray, 0, bufferArray.length);
		super.flush();
		buffer = new StringBuffer(bufferSize);
	}
}
