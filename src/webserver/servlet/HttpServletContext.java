package webserver.servlet;

import javax.servlet.ServletContext;
import javax.servlet.Servlet;
import javax.servlet.RequestDispatcher;
import java.util.HashMap;
import java.util.Enumeration;
import java.util.Collections;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

//each web application has one servletContext object, all the servlets in this web application shares it
public class HttpServletContext implements ServletContext {
	private HashMap<String,Object> attributes;
	private HashMap<String,String> initParams;//parameters in "context-param" field in web.xml
	private Logger logger;
	
	public HttpServletContext() {
		attributes = new HashMap<>();
		initParams = new HashMap<>();
		PropertyConfigurator.configure("conf/log4j.properties");
		logger = Logger.getLogger("ServerLogger");
	}
	
	@Override
	public Object getAttribute(String name) {
		return attributes.get(name);
	}
	
	@Override
	public Enumeration getAttributeNames() {
		return Collections.enumeration(attributes.keySet());
	}
	
	@Override
	public ServletContext getContext(String name) {
		return null;
	}
	
	@Override
	public String getInitParameter(String name) {
		return initParams.get(name);
	}
	
	@Override
	public Enumeration getInitParameterNames() {
		return Collections.enumeration(initParams.keySet());
	}
	
	@Override
	public int getMajorVersion() {
		return 2;
	}
	
	@Override
	public String getMimeType(String file) {
		return null;
	}
	
	@Override
	public int getMinorVersion() {
		return 4;
	}
	
	@Override
	public RequestDispatcher getNamedDispatcher(String name) {
		return null;
	}
	
	@Override
	public String getRealPath(String path) {
		return null;
	}
	
	@Override
	public RequestDispatcher getRequestDispatcher(String name) {
		return null;
	}
	
	@Override
	public java.net.URL getResource(String path) {
		return null;
	}
	
	@Override
	public java.io.InputStream getResourceAsStream(String path) {
		return null;
	}
	
	@Override
	public java.util.Set getResourcePaths(String path) {
		return null;
	}
	
	@Override
	public String getServerInfo() {
		return "Mini-Tomcat/1.0";
	}
	
	@Override
	public Servlet getServlet(String name) {//deprecated
		return null;
	}
	
	@Override
	public String getServletContextName() {
		return "WebApplicationDemo1";
	}
	
	@Override
	public Enumeration getServletNames() {//deprecated
		return null;
	}
	
	@Override
	public Enumeration getServlets() {//deprecated
		return null;
	}
	
	@Override
	public void log(Exception exception, String msg) {//deprecated
	}
	
	@Override
	public void log(String message) {
		logger.error(message);
	}
	
	@Override
	public void log(String message, Throwable throwable) {
		logger.error(message, throwable);
	}
	
	@Override
	public void removeAttribute(String name) {
		attributes.remove(name);
	}
	
	@Override
	public void setAttribute(String name, Object object) {
		attributes.put(name, object);
	}
	
	public void setInitParam(String name, String value) {
		initParams.put(name, value);
	}
}
