package webserver.servlet;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import java.util.HashMap;
import java.util.Enumeration;
import java.util.Collections;

//each servlet has its own servletConfig object
public class HttpServletConfig implements ServletConfig {
	private String name;
	private HttpServletContext servletContext;
	private HashMap<String,String> initParams;//parameters in "init-param" field(if exists) in each "servlet" field in web.xml
	
	public HttpServletConfig(String name, HttpServletContext servletContext) {
		this.name = name;
		this.servletContext = servletContext;
		initParams = new HashMap<String,String>();
	}

	@Override
	public String getInitParameter(String name) {
		return initParams.get(name);
	}
	
	@Override
	public Enumeration getInitParameterNames() {
		return Collections.enumeration(initParams.keySet());
	}
	
	@Override
	public ServletContext getServletContext() {
		return servletContext;
	}
	
	@Override
	public String getServletName() {
		return name;
	}

	public void setInitParam(String name, String value) {
		initParams.put(name, value);
	}
}
