package webserver.threadpool;

import java.util.LinkedList;
import org.apache.log4j.Logger;

import com.sun.org.apache.xalan.internal.xsltc.compiler.sym;
import com.sun.swing.internal.plaf.synth.resources.synth;

//bounded type parameters in generics, here means the element of this object can only be a class that has implemented Runnable
//https://docs.oracle.com/javase/tutorial/java/generics/bounded.html
public class TaskBlockingQueue<Processor> {
	
	private LinkedList<Processor> taskQueue;
	private Logger logger;
	
	public TaskBlockingQueue(){
		this.taskQueue = new LinkedList<>();
	}
	
	/*
	public synchronized boolean isEmpty(){
		
		boolean result;
		
		result = taskQueue.isEmpty();
		this.notifyAll();
		
		return result;
	}
	*/
	
	public synchronized void enqueue(Processor task){
		
		taskQueue.add(task);
		this.notifyAll();
		
	}
	
	public synchronized Processor dequeue(){
		Processor result = null;
		
		while(taskQueue.isEmpty()){
			try{
				//System.out.println(Thread.currentThread().getId() + " wait");
				this.wait();
				//taskQueue.notifyAll();
			}catch(InterruptedException ex){
				Thread.currentThread().interrupt();
			}
		}
		result = taskQueue.poll();
		this.notifyAll();
		return result;//it's possible that task == null, the user should deal with this situation
	}
	
	public void setLogger(Logger logger){
		this.logger = logger;
	}
}
