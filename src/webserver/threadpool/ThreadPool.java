package webserver.threadpool;

import webserver.http.Processor;

import java.io.PrintWriter;
import java.util.HashMap;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import org.apache.log4j.Logger;

public class ThreadPool{
	public static final ConcurrentHashMap<String, String> taskLog = new ConcurrentHashMap<>();
	private boolean stop = false;
	private static final int MINWorkerNumber = 1;
	private static final int MAXWorkerNumber = 100;
	private static final int DEFAULTWorkerNumber = 40;
	private int curWorkerNumber;
	private final TaskBlockingQueue<Processor> taskQueue;
	private final ConcurrentLinkedQueue<Worker> workerPool;
	private Logger logger;
	private int count;
	
	public ThreadPool(){
		this(DEFAULTWorkerNumber);
	}
	
	public ThreadPool(int WorkerNumber){
		taskQueue = new TaskBlockingQueue<>();
		taskQueue.setLogger(logger);
		workerPool = new ConcurrentLinkedQueue<>();
		this.curWorkerNumber = (WorkerNumber <= MAXWorkerNumber) ? ((WorkerNumber >= MINWorkerNumber) ? WorkerNumber : MINWorkerNumber) : MAXWorkerNumber;
		for(int i = 0; i < curWorkerNumber; i++){
			Worker worker = new Worker(taskQueue);
			worker.setLogger(logger);
			workerPool.add(worker);
			Thread thread = new Thread(worker);
			//the handler here will catch all sorts of RuntimeException thrown by the thread it is binded to
			Thread.UncaughtExceptionHandler handler = new Thread.UncaughtExceptionHandler(){ 
				@Override
				public void uncaughtException(Thread t, Throwable ex){
					//process the shutdown signal thrown from processor
					if(ex.getMessage().equals("stopped!")) shutDown();
					//deal with other RuntimeExceptions
					else ex.printStackTrace();
				}
			};
			thread.setUncaughtExceptionHandler(handler);
			taskLog.put(thread.getName(), "Parsing URL");
			thread.start();
		}
	}
	
	public void addTask(Processor task){
		if(task != null){
			taskQueue.enqueue(task);
			count++;
			System.out.println(Thread.currentThread().getId() + " addtask, total " + count);
		}
	}
	
	public boolean addWorker(){
		if(curWorkerNumber < MAXWorkerNumber){
			Worker worker = new Worker(taskQueue);
			worker.setLogger(logger);
			workerPool.add(worker);
			Thread thread = new Thread(worker);
			Thread.UncaughtExceptionHandler handler = new Thread.UncaughtExceptionHandler(){ 
				@Override
				public void uncaughtException(Thread t, Throwable ex){
					shutDown();
				}
			};
			thread.setUncaughtExceptionHandler(handler);
			thread.start();
			return true;
		}
		return false;
	}
	
	public boolean removeWorker(){
		if(curWorkerNumber > MINWorkerNumber){
			workerPool.poll().shutDown();
			return true;
		}
		return false;
	}
	
	public void shutDown(){
		if(!stop){
			for(Worker worker : workerPool){
				worker.shutDown();
			}
			/*
			Thread[] threadGroup = new Thread[Thread.activeCount()];
			Thread.enumerate(threadGroup);
			for(Thread thread : threadGroup){
				if(!thread.getName().equals("main")) thread.interrupt();
			}
			*/
		}
		stop = true;
	}
	
	public boolean isStopped(){
		return stop;
	}
	
	public void setLogger(Logger logger){
		this.logger = logger;
	}
	
	public boolean isWorkerNumberMax(){
		return curWorkerNumber == MAXWorkerNumber;
	}
}

