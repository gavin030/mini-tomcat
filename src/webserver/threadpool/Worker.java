package webserver.threadpool;

import java.util.HashMap;
import org.apache.log4j.Logger;

import webserver.http.Processor;

public class Worker implements Runnable{
	
	private volatile boolean stop = false;
	private TaskBlockingQueue<Processor> taskQueue;
	private Processor task = null;
	private Logger logger;
	private int count = 0;
	
	public Worker(TaskBlockingQueue<Processor> taskQueue){
		this.taskQueue = taskQueue;
	}
	
	@Override
	public void run(){
		while(!stop){
			/*the synchronized operation is done in the container TaskBlockingQueue, 
			 * so the classes that use it needn't deal with synchronization
			Task task = null;
			while(taskQueue.isEmpty()){
				try{
					taskQueue.wait();
					//taskQueue.notifyAll();
				}catch(InterruptedException ex){
						Thread.currentThread().interrupt();
				}
			}
			task = taskQueue.dequeue();
			if(task != null){
				task.run();
			}
			*/
			task = taskQueue.dequeue();
			count++;
			System.out.println(Thread.currentThread().getId() + " run, total " + count);
			if(task != null){
				//task.setLogger(logger);
				try{
					task.run();
				}catch(RuntimeException ex){
					shutDown();
					throw ex;
				}
			}
		}
	}
	
	public void shutDown(){
		if(task != null) task.stop();
		stop = true;
	}
	
	public void setLogger(Logger logger){
		this.logger = logger;
	}
}
