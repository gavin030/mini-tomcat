package webserver.http;

import webserver.servlet.HttpServletContext;

import java.util.Date;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionContext;
import javax.xml.crypto.Data;

public class Session implements HttpSession {
	private HttpServletContext servletContext;
	private int timeout;
	private HashMap<String, Object> attributes;
	private Date date;
	private long creationTime;
	private long lastAccessedTime;
	private String JSessionId;
	private boolean isvalid = true;
	private boolean isnew = true;
	
	public Session(HttpServletContext servletContext, int timeout){
		this.servletContext = servletContext;
		this.timeout = timeout;
		date = new Date();
		creationTime = date.getTime();
		JSessionId = String.valueOf(creationTime);
		lastAccessedTime = date.getTime();
		attributes = new HashMap<>();
	}

	@Override
	public long getCreationTime() {
		return creationTime;
	}

	@Override
	public String getId() {
		return JSessionId;
	}

	@Override
	public long getLastAccessedTime() {
		return lastAccessedTime;
	}

	@Override
	public ServletContext getServletContext() {
		return servletContext;
	}

	@Override
	public void setMaxInactiveInterval(int timeout) {
		this.timeout = timeout;
	}

	@Override
	public int getMaxInactiveInterval() {
		return timeout;
	}

	@Override
	public HttpSessionContext getSessionContext() {//deprecated
		return null;
	}

	@Override
	public Object getAttribute(String name) {
		return attributes.get(name);
	}

	@Override
	public Object getValue(String name) {//deprecated
		return null;
	}

	@Override
	public Enumeration getAttributeNames() {
		return Collections.enumeration(attributes.keySet());
	}

	@Override
	public String[] getValueNames() {//deprecated
		return null;
	}

	@Override
	public void setAttribute(String name, Object object) {
		attributes.put(name, object);
	}

	@Override
	public void putValue(String arg0, Object arg1) {//deprecated
	}

	@Override
	public void removeAttribute(String name) {
		attributes.remove(name);
	}

	@Override
	public void removeValue(String arg0) {//deprecated
	}

	@Override
	public void invalidate() {
		isvalid = false;
	}

	@Override
	public boolean isNew() {
		return isnew;
	}

	public boolean isValid() {
		if(!isvalid) return false;
		long tempTime = date.getTime();
		if((int)((tempTime - lastAccessedTime) / 1000) <= timeout){
			isnew = false;
			lastAccessedTime = tempTime;
		}else invalidate();
		return isvalid;
	}
	
	public void setSessionId(String JSessionId){
		this.JSessionId = JSessionId;
	}
}
