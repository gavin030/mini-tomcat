package webserver.http;

import webserver.threadpool.ThreadPool;
import webserver.stream.BufferPrintWriter;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Method;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Locale;

import javax.print.attribute.standard.RequestingUserName;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;


public class HttpResponse implements HttpServletResponse{

	private HttpRequest request;
	private OutputStream output;
	private Path root;
	private Path path;
	private BufferPrintWriter writer;
	private int bufferSize = 1024;
	private boolean useOutputStream;
	private boolean writerinuse;
	public boolean useServlet;
	
	private boolean committed;
	private int statusCode;
	private String characterEncoding = "ISO-8859-1";
	private HashMap<String, LinkedList<String>> headers;
	private LinkedList<Cookie> cookies;
	private Locale locale;
	
	public HttpResponse(HttpRequest request, OutputStream output, String root) {
		this.request = request;
		this.output = output;
		this.root = Paths.get(root);
		try{
			//use outputStreamWriter so that to set characterEncoding
			writer = new BufferPrintWriter(new OutputStreamWriter(output, characterEncoding), bufferSize);//new PrintWriter(new BufferedWriter(new OutputStreamWriter(output, characterEncoding), bufferSize));//do not autoflush
		}catch(UnsupportedEncodingException ex){
			ex.printStackTrace();
		}
		useOutputStream = false;
		writerinuse = false;
		useServlet = false;
		committed = false;
		statusCode = 0;
		headers = new HashMap<>();
		headers.put("content-length", new LinkedList<String>());//all the headers are kept in lower case, and they will be recovered to capital case when outputing
		headers.get("content-length").add(String.valueOf(-1));
		headers.put("content-type", new LinkedList<String>());
		headers.get("content-type").add("text/html");
		cookies = new LinkedList<>();
	}
	
	@Override
	public void flushBuffer(){
		writer.flush();
	}

	@Override
	public int getBufferSize() {
		return bufferSize;
	}

	@Override
	public String getCharacterEncoding() {
		return characterEncoding;
	}

	@Override
	public String getContentType() {
		String key = "content-type";
		return headers.get(key).peek();
	}

	@Override
	public Locale getLocale() {
		return locale;
	}

	public OutputStream getOutput(){
		return output;
	}
	
	@Override
	public ServletOutputStream getOutputStream() throws IOException {
		return null;
	}
	
	public Path getPath(){
		return path;
	}
	
	public String getProtocol(){
		return request.getProtocol();
	}
	
	public String getReasonPhrase(){
		//https://www.w3.org/Protocols/rfc2616/rfc2616-sec6.html 6.1.1
		switch(statusCode){
		case SC_OK://200
	        return "OK";
		case SC_ACCEPTED://202
			return "Accepted";
		case SC_BAD_GATEWAY://502
	        return "Bad Gateway";
		case SC_BAD_REQUEST://400
	        return "Bad Request";
		case SC_CONFLICT://409
	        return "Conflict";
		case SC_CONTINUE://100
	        return "Continue";
	    case SC_CREATED://201
	        return "Created";
	    case SC_EXPECTATION_FAILED://417
	        return "Expectation Failed";
	    case SC_FORBIDDEN://403
	        return "Forbidden";
	    case SC_GATEWAY_TIMEOUT://504
	        return "Gateway Timeout";
	    case SC_GONE://410
	        return "Gone";
	    case SC_HTTP_VERSION_NOT_SUPPORTED://505
	        return "HTTP Version Not Supported";
	    case SC_INTERNAL_SERVER_ERROR://500
	        return "Internal Server Error";
	    case SC_LENGTH_REQUIRED://411
	        return "Length Required";
	    case SC_METHOD_NOT_ALLOWED://405
	        return "Method Not Allowed";
	    case SC_MOVED_PERMANENTLY://301
	        return "Moved Permanently";
	    case SC_MOVED_TEMPORARILY://302
	        return "Moved Temporarily";
	    case SC_MULTIPLE_CHOICES://300
	        return "Multiple Choices";
	    case SC_NO_CONTENT://204
	        return "No Content";
	    case SC_NON_AUTHORITATIVE_INFORMATION://203
	        return "Non-Authoritative Information";
	    case SC_NOT_ACCEPTABLE://406
	        return "Not Acceptable";
	    case SC_NOT_FOUND://404
	        return "Not Found";
	    case SC_NOT_IMPLEMENTED://501
	        return "Not Implemented";
	    case SC_NOT_MODIFIED://304
	        return "Not Modified";
	    case SC_PARTIAL_CONTENT://206
	        return "Partial Content";
	    case SC_PAYMENT_REQUIRED://402
	        return "Payment Required";
	    case SC_PRECONDITION_FAILED://412
	        return "Precondition Failed";
	    case SC_PROXY_AUTHENTICATION_REQUIRED://407
	        return "Proxy Authentication Required";
	    case SC_REQUEST_ENTITY_TOO_LARGE://413
	        return "Request Entity Too Large";
	    case SC_REQUEST_TIMEOUT://408
	        return "Request Timeout";
	    case SC_REQUEST_URI_TOO_LONG://414
	        return "Request URI Too Long";
	    case SC_REQUESTED_RANGE_NOT_SATISFIABLE://416
	        return "Requested Range Not Satisfiable";
	    case SC_RESET_CONTENT://205
	        return "Reset Content";
	    case SC_SEE_OTHER://303
	        return "See Other";
	    case SC_SERVICE_UNAVAILABLE://503
	        return "Service Unavailable";
	    case SC_SWITCHING_PROTOCOLS://101
	        return "Switching Protocols";
	    case SC_TEMPORARY_REDIRECT://307
	    	return "Temporary Redirect";
	    case SC_UNAUTHORIZED://401
	        return "Unauthorized";
	    case SC_UNSUPPORTED_MEDIA_TYPE://415
	        return "Unsupported Media Type";
	    case SC_USE_PROXY://305
	        return "Use Proxy";
	    default:
	        return "HTTP Response Status " + statusCode;
		}
	}

	@Override
	public PrintWriter getWriter(){// throws IOException {
		return writer;
	}

	@Override
	public boolean isCommitted() {
		return committed;
	}
	
	public void commit(){
		committed = true;
	}

	@Override
	public void reset() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resetBuffer() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setBufferSize(int size) {//TODO
		if(writerinuse) return;//buffersize can not be changed if the writer is being written
		this.bufferSize = size;
	}

	@Override
	public void setCharacterEncoding(String characterEncoding) {
		this.characterEncoding = characterEncoding;
	}

	@Override
	public void setContentLength(int len) {
		setIntHeader("content-length", len);
	}
	
	public void setContentLengthLong(long len){
		setHeader("content-length", String.valueOf(len));
	}

	@Override
	public void setContentType(String type) {
		setHeader("content-type", type);
	}

	@Override
	public void setLocale(Locale locale) {
		this.locale = locale;
		String language = locale.getLanguage();
		String country = locale.getCountry();
		if(language != null){
			if(country != null) addHeader("content-language", language + "-" + country);
			else addHeader("content-language", language);
		}
	}

	@Override
	public void addCookie(Cookie cookie) {
		cookies.add(cookie);
	}

	@Override
	public void addDateHeader(String name, long value) {
		addHeader(name, new Date(value).toString());
	}

	@Override
	public void addHeader(String name, String value) {
		LinkedList<String> values = headers.get(name);
		if(values == null){
			values = new LinkedList<>();
			headers.put(name, values);
		}
		values.add(value);
	}

	@Override
	public void addIntHeader(String name, int value) {
		addHeader(name, String.valueOf(value));
	}

	@Override
	public boolean containsHeader(String name) {
		return headers.get(name) != null;
	}

	@Override
	public String encodeRedirectURL(String arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String encodeRedirectUrl(String arg0) {// Deprecated
		return null;
	}

	@Override
	public String encodeURL(String arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String encodeUrl(String arg0) {// Deprecated
		return null;
	}

	@Override
	public void sendError(int status) throws IOException {
		String errorMessage;
		switch(status){
		case SC_NOT_FOUND:
			errorMessage = "<h1>404 Not Found</h1>";
			writer.print(errorMessage);
			break;
		case SC_BAD_REQUEST:
			errorMessage = "<h1>Bad Request</h1>";
			writer.print(errorMessage);
		default:
			errorMessage = "<h1>404 Not Found</h1>";
			writer.print(errorMessage);
			break;
		}
	}

	@Override
	public void sendError(int status, String message) throws IOException {
		writer.println(status + " " + message);
	}

	@Override
	public void sendRedirect(String arg0) throws IOException {
		// TODO Auto-generated method stub
		
	}
	
	public void judgeOutputMethod(){
		if(!useServlet && statusCode != 0 && statusCode != SC_NOT_FOUND && statusCode != SC_BAD_REQUEST &&
		   !request.getRequestURI().startsWith("/control") && !Files.isDirectory(path)){
			String MIME = getContentType();
			if(!MIME.equals("text/plain") && !MIME.equals("text/html")) useOutputStream = true;
		}
	}
	
	public void sendStatusLine(){
		String statusLineString = getProtocol() + " " + statusCode;
		if(getReasonPhrase() != null) statusLineString += " " + getReasonPhrase();
		statusLineString += "\r\n";
		if(useOutputStream){
			byte[] statusLineBytes = statusLineString.getBytes();
			try{
				output.write(statusLineBytes, 0, statusLineBytes.length);
			}catch(IOException ex){
				ex.printStackTrace();
			}
		}
		else{
			if(writer == null) return;
			writerinuse = true;
			writer.print(statusLineString);
		}
	}
	
	public void sendHeaders(){
		
	}
	
	public void sendCRLF(){
		String CRLF = "\r\n";
		if(useOutputStream){
			byte[] CRLFBytes = CRLF.getBytes();
			try{
				output.write(CRLFBytes, 0, CRLFBytes.length);
			}catch(IOException ex){
				ex.printStackTrace();
			}
		}
		else{
			if(writer == null) return;
			writer.print(CRLF);
		}
	}
	
	public void sendControlPanel(){
		writer.printf("<!DOCTYPE html>%n");
		writer.printf("<html>%n");
		writer.printf("<head>%n");
		writer.printf("<title>Control Panel</title>%n");
		writer.printf("</head>%n");
		writer.printf("<body>%n");
		Thread[] threadGroup = new Thread[Thread.activeCount()];
		Thread.enumerate(threadGroup);
		
		for(Thread thread : threadGroup){
			if(thread.getName().equals("main")) continue;
			writer.printf("<a>");//<a> tag in HTML default method is get
			writer.printf("%s	", String.format("%-50s", thread.getName()));
			String stateInfo;
			switch(thread.getState()){
				case WAITING:
					stateInfo = "waiting";
					break;
				case TIMED_WAITING:
					stateInfo = "waiting";
					break;
				case TERMINATED:
					stateInfo = "terminated";
					break;
				case NEW:
					stateInfo = ThreadPool.taskLog.get(thread.getName());
					break;
				case RUNNABLE:
					stateInfo = ThreadPool.taskLog.get(thread.getName());
					break;
				case BLOCKED:
					stateInfo = ThreadPool.taskLog.get(thread.getName());
					break;
				default:
					stateInfo = ThreadPool.taskLog.get(thread.getName());
					break;
			}
			writer.printf("%s", stateInfo);
			writer.printf("</a><br/>%n");
		}
		writer.printf("<input type=\"button\"");
		writer.printf("value=\"shutdown\"");
		writer.printf("onclick=\"location.href('/shutdown')\"></a>%n");
		writer.printf("</body>%n");
		writer.printf("</html>%n");
		
	}
	
	public void sendDirectory(){
		writer.printf("<!DOCTYPE html>%n");
		writer.printf("<html>%n");
		writer.printf("<head>%n");
		writer.printf("<title>Directory</title>%n");
		writer.printf("</head>%n");
		writer.printf("<body>%n");
		try(DirectoryStream<Path> subPaths = Files.newDirectoryStream(path)){
			for(Path subPath : subPaths){
				String pathName = subPath.getFileName().toString();
				String fromRoot = root.relativize(path).toString();
				if(!fromRoot.startsWith("/") && !fromRoot.equals("")) fromRoot = "/" + fromRoot;
				writer.printf("<a href=\"%s\">", fromRoot +"/"+pathName);//<a> tag in HTML default method is get
				writer.printf("%s", pathName);
				writer.printf("</a><br/>%n");
			}
		}catch(IOException ex){
			ex.printStackTrace();
		}finally{
			writer.printf("</body>%n");
			writer.printf("</html>%n");
		}
	}
	
	public void sendFile(){
		String MIME = getContentType();
		if(MIME.equals("text/plain") || MIME.equals("text/html")){
			BufferedReader fileReader = null;
			try{
				fileReader = Files.newBufferedReader(path);
				String line = fileReader.readLine();
				while(line != null){
					writer.print(line);
					line = fileReader.readLine();
				}
				if(fileReader != null) fileReader.close();
			}catch(IOException ex){
				ex.printStackTrace();
			}
		}
		else{// if(MIME.equals("image/jpeg") || MIME.equals("image/png") || MIME.equals("image/gif")){
			InputStream fileInput = null;
			try{
				fileInput = Files.newInputStream(path);
				byte[] localBuffer = new byte[bufferSize];
				int ch = fileInput.read(localBuffer, 0, bufferSize);
				while(ch != -1){
					output.write(localBuffer, 0, ch);
					ch = fileInput.read(localBuffer, 0, bufferSize);
				}
				if(fileInput != null) fileInput.close();
			}catch(IOException ex){
				ex.printStackTrace();
			}
		}
	}
	
	public void sendStaticResource(){
		if(statusCode != 0 && statusCode != SC_NOT_FOUND && statusCode != SC_BAD_REQUEST && !path.toString().startsWith("/control")){
			if(Files.isDirectory(path)){
				sendDirectory();
			}
			else{
				sendFile();
			}
		}
		else if(path != null && path.toString().startsWith("/control")){
			sendControlPanel();
		}
		else{
			try{
				sendError(statusCode);
			}catch(IOException ex){
				ex.printStackTrace();
			}
		}
		finishResponse();
	}
	
	public void finishResponse(){
		committed = true;
		if(writer != null){
			writer.flush();
			writer.close();
		}
	}

	@Override
	public void setDateHeader(String name, long value) {
		setHeader(name, new Date(value).toString());
	}

	@Override
	public void setHeader(String name, String value) {
		headers.put(name, new LinkedList<String>());
		headers.get(name).add(value);
	}

	@Override
	public void setIntHeader(String name, int value) {
		setHeader(name, String.valueOf(value));
	}
	
	public void setPath(Path path){
		this.path = path;
	}

	@Override
	public void setStatus(int statusCode) {
		this.statusCode = statusCode;
	}

	@Override
	public void setStatus(int arg0, String arg1) {// Deprecated
	}

}
