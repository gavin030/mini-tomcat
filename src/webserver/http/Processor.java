package webserver.http;

import webserver.HttpServer;
import webserver.threadpool.ThreadPool;
import webserver.http.HttpRequest;
import webserver.http.HttpResponse;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.Reader;
import java.net.Socket;
import java.nio.file.Files;
import java.nio.file.InvalidPathException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Locale;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import com.sun.corba.se.spi.orbutil.threadpool.Work;


public class Processor{
	
	private Socket socket;
	private String root;
	private HttpRequest request;
	private HttpResponse response;
	private BufferedReader input;//Only BufferedReader has readLine() method among the readers
	private OutputStream output;
	private String servletName;
	private boolean stopped = false;
	private Logger logger;
	
	public Processor(Socket socket, String root) {
		this.socket = socket;
		this.root = root;
		try{
			input = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			output = socket.getOutputStream();
		}catch(IOException ex){
			ex.printStackTrace();
		}
		request = new HttpRequest(socket, input);
		//request.setLogger(logger);
		response = new HttpResponse(request, output, root);
		//response.setLogger(logger);
		
		PropertyConfigurator.configure("conf/log4j.properties");
		logger = Logger.getLogger("ServerLogger");
	}
	
	public void processRequest(){
		
		int count = 0;
		//process initialLine
		String line = null;
		try{
			line = input.readLine();
			/*for debugging
			System.out.println("at line " + (++count));
			System.out.println(line);
			*/
		}catch(IOException ex){
			System.out.println(Thread.currentThread().getName() + "socket is closed");
			//ex.printStackTrace();
			stop();
			return;
		}
		if(line != null) parseInitialLine(line);
		
		//process headerLines
		String lastLine = null;
		try{
			line = input.readLine();
			/*for debugging
			System.out.println("at line " + (++count));
			System.out.println(line);
			*/
		}catch(IOException ex){
			ex.printStackTrace();
			stop();
			return;
		}
		while(true){
			//System.out.println(line);
			if(line != null && !line.startsWith(" ")){
				if(lastLine != null) parseHeaderLine(lastLine);
				lastLine = line;
			}
			else if(line != null){
				lastLine = lastLine.trim() + " " + line.trim();
			}
			
			try{
				line = input.readLine();
				/*for debugging
				System.out.println("at line " + (++count));
				System.out.println(line);
				System.out.println("last line");
				System.out.println(lastLine);
				System.out.println(line.equals("\r\n"));
				System.out.println(line.equals("\n"));
				System.out.println(line.equals("\r"));
				System.out.println(line.equals(""));
				*/
				//if request has no body, the empty line will be "" instead of CRLF or LF
				if(line == null || line.equals("\r\n") || line.equals("\n") || line.equals("")) break;
			}catch(IOException ex){
				ex.printStackTrace();
				stop();
				return;
			}
		}
		/*for debugging
		System.out.println("final last line");
		System.out.println(lastLine);
		*/
		if(lastLine != null) parseHeaderLine(lastLine);//don't forget to parse the last headerLine!
		
		ThreadPool.taskLog.put(Thread.currentThread().getName(), request.getRequestURL().toString());
		
		//process request body
		
	}
	
	public void parseInitialLine(String initialLine){//still need to deal with CRLF at the end
		initialLine = initialLine.trim() + "\r\n";//in case of the SPs at the beginning of initialLine
		String URI = null;
		int begin = 0;
		int end = 0;
		int count = 0;
		while(true){
			if(initialLine.charAt(end) != ' ' && initialLine.charAt(end) != '\r' && initialLine.charAt(end) != '\n'){
				end++;
			}
			else{
				if(count == 0){//parse method
					request.setMethod(initialLine.substring(begin, end).toUpperCase());
					begin = end;
					while(initialLine.charAt(begin) == ' '){
						begin++;
					}
					end = begin;
					count++;
				}
				else if(count == 1){//parse host, URI and queryString
					int queryIndex = initialLine.indexOf("?");
					//divide URI and queryString
					if(queryIndex == -1) URI = initialLine.substring(begin, end);
					else{
						//in case that there are SPs between URI and "?" or "?" and queryString
						URI = initialLine.substring(begin, queryIndex).trim();
						request.setQueryString(initialLine.substring(queryIndex+1, end).trim());
					}
					//parse absolute URI into host and relative URI
					if(!URI.startsWith("/")){
						int p1 = initialLine.indexOf("://");
						int p2 = initialLine.indexOf("/", p1+3);
						int p3 = initialLine.indexOf(":", p1+1);//the index of port
						if(p2 == -1){//if absolute URI does not have relative URI
							if(p3 == -1) request.addHeader("Host", URI.substring(p1+3));
							else request.addHeader("Host", URI.substring(p1+3, p3).trim());
							URI = "";
						}
						else{
							if(p3 != -1 && p3 < p2) request.addHeader("Host", URI.substring(p1+3, p3).trim());
							else request.addHeader("Host", URI.substring(p1+3, p2).trim());
							URI = URI.substring(p2);
						}
					}
					request.setRequestURI(URI);
					begin = end;
					while(initialLine.charAt(begin) == ' '){
						begin++;
					}
					end = begin;
					count++;
				}
				else{//parse protocol
					request.setProtocol(initialLine.substring(begin, end));
					break;
				}
			}
		}
	}
	
	public void parseHeaderLine(String headerLine){
		//System.out.println(headerLine);
		int middle = headerLine.indexOf(':');
		String header = headerLine.substring(0, middle).trim();
		String value = headerLine.substring(middle+1, headerLine.length()).trim();
		request.addHeader(header, value);
		
		//parse "Accept-Language" and set it into locales in the request
		//https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Accept-Language
		if(header.equals("Accept-Language")){
			LinkedList<Locale> locales = new LinkedList<>();
			for(String language : value.split(",")){
				int i = language.indexOf(";");
				if(i != -1) language = language.substring(0, i).trim();//delete the quality value part
				i = language.indexOf("-");
				if(i == -1) locales.add(new Locale(language));//has only language
				else locales.add(new Locale(language.substring(0, i).trim(), language.substring(i+1).trim()));//has both language and country
			}
			request.setLocales(locales);
		}
		if(header.equals("Cookie")){
			for(String cookie : value.split(";")){
				int i = cookie.indexOf("=");
				String name = cookie.substring(0, i).trim();
				String val = cookie.substring(i+1).trim();
				request.addCookie(name, val);
				if(name.toLowerCase().equals("jsessionid")) request.setSessionId(val);
			}
			Session session = (Session)request.getSession(true);
			for(Cookie cookie : request.getCookies()){
				session.setAttribute(cookie.getName(), cookie.getValue());
			}
			response.addCookie(new Cookie("JSESSIONID", session.getId()));
		}
	}
	
	//Filling in headers in processor, and judging and sending resource in httpResponse
	public void fillInResponse(){
		if(stopped) return;
		//return bad request if HTTP/1.1 request doesn't include "Host" header
		if(request.getProtocol().toUpperCase().equals("HTTP/1.1") && request.getHeader("Host") == null){
			response.setContentType("text/html");
			response.setContentLength(20);
			response.setStatus(400);
			response.setPath(null);
			return;
		}
		
		String URI = request.getRequestURI();
		if(URI.startsWith("/control")){
			response.setContentType("text/html");
			response.setStatus(200);
			response.setPath(Paths.get(URI));
			return;
		}
		
		if(HttpServer.servletNames.containsKey(URI)){//URI in servletNames
			if(URI.endsWith("/")) request.setServletPath(URI.substring(0, URI.length()-1));
			else request.setServletPath(URI);
			response.setContentType("text/html");
			response.setStatus(200);
			response.useServlet = true;
			servletName = HttpServer.servletNames.get(URI);
			return;
		}else if(URI.endsWith("/") && HttpServer.servletNames.containsKey(URI.substring(0, URI.length()-1))){//URI-"/" in servletNames
			request.setServletPath(URI.substring(0, URI.length()-1));
			response.setContentType("text/html");
			response.setStatus(200);
			response.useServlet = true;
			servletName = HttpServer.servletNames.get(URI.substring(0, URI.length()-1));
			return;
		}else if(!URI.endsWith("/") && HttpServer.servletNames.containsKey(URI + "/")){//URI+"/" in servletNames
			request.setServletPath(URI);
			response.setContentType("text/html");
			response.setStatus(200);
			response.useServlet = true;
			servletName = HttpServer.servletNames.get(URI + "/");
			return;
		}else{
			for(String urlPattern : HttpServer.servletNames.keySet()){
				if(urlPattern.endsWith("/*") && (URI + "/").startsWith(urlPattern.substring(0, urlPattern.length()-1))){
					if(urlPattern.equals("/*")) request.setServletPath("");
					else request.setServletPath(urlPattern.substring(0, urlPattern.length()-2));
					response.setContentType("text/html");
					response.setStatus(200);
					response.useServlet = true;
					servletName = HttpServer.servletNames.get(urlPattern);
					return;
				}
			}
		}
		
		Path path = null;
		try{
			path = Paths.get(root, URI);
		}catch(InvalidPathException ex){
			ex.printStackTrace();
			response.setContentType("text/html");
			response.setContentLength(22);
			response.setStatus(404);
			response.setPath(null);
			return;
		}
		response.setPath(path);
		if(path != null && Files.exists(path)){
			if(Files.isDirectory(path)){
				response.setContentType("text/html");
				response.setStatus(200);
			}
			else{
				String type = null;
				try{
					type = Files.probeContentType(path);
				}catch(IOException ex){
					ex.printStackTrace();
				}
				response.setContentType(type);
				response.setContentLengthLong((new File(path.toString())).length());
				response.setStatus(200);
			}
		}
		else{
			response.setContentType("text/html");
			response.setContentLength(22);
			response.setStatus(404);
		}
	}
	
	public void executeServlet(String servletName){
		String className = HttpServer.servletClassNames.get(servletName);
		HttpServlet servlet = null;
		try{
			Class servletClass = Class.forName(className);
			servlet = (HttpServlet) servletClass.newInstance();
			servlet.init(HttpServer.servletConfigs.get(servletName));
			logger.info("servlet init succeed!");
			logger.info(servlet);
			servlet.service(request, response);
		}catch(ClassNotFoundException | InstantiationException | 
				IllegalAccessException | ServletException | 
				IOException ex){
			ex.printStackTrace();
		}
		response.flushBuffer();
		response.commit();
		servlet.destroy();
	}
	
	public void sendResponse(){
		if(stopped) return;
		if(response.isCommitted()) return;
		response.judgeOutputMethod();
		response.sendStatusLine();
		response.sendHeaders();
		response.sendCRLF();
		if(servletName != null) executeServlet(servletName);
		else response.sendStaticResource();//send message body
	}
	
	public void run(){
		processRequest();
		if(request.getRequestURI().startsWith("/shutdown")){
			stop();
			throw new RuntimeException("stopped!");
		}
		fillInResponse();
		sendResponse();
		try{
			socket.close();
		}catch(IOException ex){
			ex.printStackTrace();
		}
		
	}
	
	public void stop(){
		stopped = true;
		if(socket != null){
			try{
				socket.close();
			}catch(IOException ex){
				ex.printStackTrace();
			}
		}
	}
	
	public void setLogger(Logger logger){
		this.logger = logger;
	}
}
