package webserver.http;

import webserver.HttpServer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.Socket;
import java.security.Principal;
import java.util.Collection;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Locale;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletInputStream;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import sun.util.logging.resources.logging;


public class HttpRequest implements HttpServletRequest{

	private Socket socket;
	
	private String method;
	private String requestURI;
	private String queryString = null;
	private String protocol;
	private String encoding = "ISO-8859-1";
	private LinkedList<Locale> locales;
	private HashMap<String, Object> attributes = new HashMap<>();
	private BufferedReader reader;
	
	private HashMap<String, String> headers;
	private LinkedList<Cookie> cookies;
	private String JSessionId;
	private String servletPath;
	
	private Logger logger;
	
	public HttpRequest(Socket socket, BufferedReader reader){
		this.socket = socket;
		this.reader = reader;
		headers = new HashMap<>();
		headers.put("Host", null);
		headers.put("Content-Length", "-1");
		locales = new LinkedList<>();
		cookies = new LinkedList<>();
	}
	
	public void addHeader(String key, String value){
		headers.put(key, value);//put() method will cover the old value if the key already exists
	}
	
	public void addCookie(String name, String value){
		cookies.add(new Cookie(name, value));
	}

	@Override
	public Object getAttribute(String name) {
		synchronized (attributes) {
			return attributes.get(name);
		}
	}

	@Override
	public Enumeration<String> getAttributeNames() {
		synchronized (attributes) {
			return Collections.enumeration(attributes.keySet());
		}
	}

	@Override
	public String getCharacterEncoding() {
		return encoding;
	}

	@Override
	public int getContentLength() {//"Content-Length" header in request
		String key = "Content-Length";
		return Integer.valueOf(headers.get(key));
	}

	//@Override
	public long getContentLengthLong() {//convert "Content-Length" from int to long
		String key = "Content-Length";
		return (long)Integer.valueOf(headers.get(key));
	}

	@Override
	public String getContentType() {//"Content-Type" header in request
		String key = "Content-Type";
		return headers.get(key);
	}

	@Override
	public ServletInputStream getInputStream() throws IOException {
		return null;
	}

	@Override
	public String getLocalAddr() {
		return "127.0.0.1";//socket.getInetAddress().getLocalHost().toString();
	}

	@Override
	public String getLocalName() {
		return "localhost";//socket.getInetAddress().getLocalHost().getHostName();
	}

	@Override
	public int getLocalPort() {
		return socket.getLocalPort();
	}

	@Override
	public Locale getLocale() {
		return locales.peekFirst();
	}

	@Override
	public Enumeration<Locale> getLocales() {
		return Collections.enumeration(locales);
	}

	@Override
	public String getParameter(String arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Map<String, String[]> getParameterMap() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Enumeration<String> getParameterNames() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String[] getParameterValues(String arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getProtocol() {
		return protocol;
	}

	@Override
	public BufferedReader getReader(){
		return reader;
	}

	@Override
	public String getRealPath(String arg0) {//deprecated
		return null;
	}

	@Override
	public String getRemoteAddr() {
		return socket.getInetAddress().toString();
	}

	@Override
	public String getRemoteHost() {
		return socket.getInetAddress().getHostName();
	}

	@Override
	public int getRemotePort() {
		return socket.getPort();
	}

	@Override
	public RequestDispatcher getRequestDispatcher(String arg0) {
		return null;
	}

	@Override
	public String getScheme() {
		return "http";
	}

	@Override
	public String getServerName() {
		String host = headers.get("Host");
		int i = host.indexOf(":");
		if(i == -1) return host;
		return host.substring(0, i).trim();
	}

	@Override
	public int getServerPort() {
		String host = headers.get("Host");
		int i = host.indexOf(":");
		if(i == -1) return getLocalPort();
		return Integer.valueOf(host.substring(i+1).trim());
	}

	@Override
	public boolean isSecure() {
		return false;
	}

	@Override
	public void removeAttribute(String name) {
		attributes.remove(name);
	}

	@Override
	public void setAttribute(String name, Object value) {
		attributes.put(name, value);
	}

	@Override
	public void setCharacterEncoding(String encoding){
		this.encoding = encoding;
	}
	
	public void setLocales(LinkedList<Locale> locales){
		this.locales = locales;
	}
	
	public void setMethod(String method){
		this.method = method;
	}
	
	public void setProtocol(String protocol){
		this.protocol = protocol;
	}
	
	public void setQueryString(String queryString){
		this.queryString = queryString;
	}
	
	public void setRequestURI(String requestURI) {
		this.requestURI = requestURI;
	}

	@Override
	public String getAuthType() {
		return BASIC_AUTH;
	}

	@Override
	public String getContextPath() {
		return "";
	}

	@Override
	public Cookie[] getCookies() {
		Cookie[] cookieArray = new Cookie[cookies.size()];
		if(!cookies.isEmpty()) return cookies.toArray(cookieArray);
		return cookieArray;
	}

	@Override
	public long getDateHeader(String arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public String getHeader(String key) {
		return headers.get(key);
	}

	@Override
	public Enumeration<String> getHeaderNames() {
		return Collections.enumeration(headers.keySet());
	}

	@Override
	public Enumeration<String> getHeaders(String arg0) {
		// TODO Auto-generated method stub
		return null;
	}
	
	

	@Override
	public int getIntHeader(String name) {
		if(!headers.containsKey(name)) return -1;
		return Integer.valueOf(headers.get(name));
	}

	@Override
	public String getMethod() {
		return this.method;
	}

	@Override
	public String getPathInfo() {
		if(servletPath == null) return null;
		String begin = getContextPath() + getServletPath();
		return requestURI.substring(begin.length());
	}

	@Override
	public String getPathTranslated() {
		return null;
	}

	@Override
	public String getQueryString() {
		return this.queryString;
	}

	@Override
	public String getRemoteUser() {
		return null;
	}

	@Override
	public String getRequestURI() {
		return requestURI;
	}

	@Override
	public StringBuffer getRequestURL() {
		return (new StringBuffer("http://localhost:")).append(getLocalPort()).append(requestURI);
	}

	@Override
	public String getRequestedSessionId() {
		return JSessionId;
	}

	@Override
	public String getServletPath() {
		return servletPath;
	}

	@Override
	public HttpSession getSession() {
		Session session;
		if(JSessionId == null){
			session = new Session(HttpServer.servletContext, HttpServer.sessionTimeout);
			JSessionId = session.getId();
			HttpServer.sessions.put(session.getId(), session);
			return session;
		}else{
			session = (Session)HttpServer.sessions.get(JSessionId);
			if(session == null || !session.isValid()){
				session = new Session(HttpServer.servletContext, HttpServer.sessionTimeout);
				session.setSessionId(JSessionId);
				HttpServer.sessions.put(session.getId(), session);
				return session;
			}else return HttpServer.sessions.get(JSessionId);
		}
	}

	@Override
	public HttpSession getSession(boolean create) {
		Session session;
		if(JSessionId == null){
			if(create){
				session = new Session(HttpServer.servletContext, HttpServer.sessionTimeout);
				HttpServer.sessions.put(session.getId(), session);
				return session;
			}
			return null;
		}else{
			session = (Session)HttpServer.sessions.get(JSessionId);
			if(session == null || !session.isValid()){
				if(create){
					session = new Session(HttpServer.servletContext, HttpServer.sessionTimeout);
					session.setSessionId(JSessionId);
					HttpServer.sessions.put(session.getId(), session);
					return session;
				}
				return null;
			}else return session;
		}
	}

	@Override
	public Principal getUserPrincipal() {
		return null;
	}

	@Override
	public boolean isRequestedSessionIdFromCookie() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isRequestedSessionIdFromURL() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isRequestedSessionIdFromUrl() {//deprecated
		return false;
	}

	@Override
	public boolean isRequestedSessionIdValid() {
		if(JSessionId != null){
			Session session = (Session)HttpServer.sessions.get(JSessionId);
			if(session != null) return session.isValid();
			return false;
		}
		return false;
	}

	@Override
	public boolean isUserInRole(String arg0) {
		return false;
	}
	
	public void setSessionId(String JSessionId){
		this.JSessionId = JSessionId;
	}
	
	public void setServletPath(String servletPath){
		this.servletPath = servletPath;
	}

	public void setLogger(Logger logger){
		this.logger = logger;
	}
	
	
}
