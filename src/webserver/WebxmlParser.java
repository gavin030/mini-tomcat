package webserver;

import webserver.servlet.HttpServletContext;
import webserver.servlet.HttpServletConfig;
import webserver.http.Session;
import webserver.http.HttpRequest;
import webserver.http.HttpResponse;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

//parsing web.xml file, loading servletContext and instantiating servlets 
public class WebxmlParser {
	private String xmlPath;
	private HashMap<String,String> servletClassNames;
	private HashMap<String, String> urlPatterns;
	private HashMap<String, HttpServletConfig> servletConfigs;
	private HttpServletContext servletContext;
	private HashMap<String,String> contextParams;
	private HashMap<String,HashMap<String,String>> servletParams;
	private int sessionTimeout;
	
	public WebxmlParser(String xmlPath, HashMap<String,String> servletClassNames, HashMap<String, String> urlPatterns, HashMap<String, HttpServletConfig> servletConfigs, HttpServletContext servletContext){
		this.xmlPath = xmlPath;
		this.servletClassNames = servletClassNames;
		this.urlPatterns = urlPatterns;
		this.servletConfigs = servletConfigs;
		this.servletContext = servletContext;
		contextParams = new HashMap<String,String>();
		servletParams = new HashMap<String,HashMap<String,String>>();
		PropertyConfigurator.configure("conf/log4j.properties");
		Logger logger = Logger.getLogger("ServerLogger");
		logger.info("xmlParser Initiated!");
	}
	
	//define the pattern of parsing xml
	private class Handler extends DefaultHandler {
		private int state = 0;
		private String servletName;
		private String paramName;
		
		//notice that the traversing of a xml file has an order
		//the parsing process is actually jumping from one state(one tag) to another state(another tag)
		public void startElement(String uri, String localName, String qName, Attributes attributes) {
			if (qName.compareTo("servlet") == 0) {
				state = 1;
			}else if(qName.equals("servlet-mapping")){
				state = 2;
			}else if (qName.equals("servlet-name")) {
				state = (state == 1) ? 10 : 20;
			}else if(qName.equals("servlet-class")){
				state = 11;
			}else if(qName.equals("url-pattern")){
				state = 21;
			}else if (qName.equals("context-param")) {
				state = 3;
			}else if (qName.equals("init-param")) {
				state = 4;
			}else if (qName.equals("param-name")) {//"context-param" and "init-param" all have "param-name" and "param-value", thus leading to such branchs
				state = (state == 3) ? 30 : 40;
			}else if (qName.equals("param-value")) {
				state = (state == 30) ? 31 : 41;
			}else if(qName.equals("session-timeout")){
				state = 5;
			}
		}
		
		public void characters(char[] ch, int start, int length) {
			String value = new String(ch, start, length);
			if (state == 10 || state == 20) {
				servletName = value;
				state = 0;
			} else if (state == 11) {
				servletClassNames.put(servletName, value);//servletName : servletClass
				state = 0;
			}else if(state == 21){ 
				urlPatterns.put(servletName, value);//servletName : urlPattern
				state = 0;
			}else if (state == 30 || state == 40) {
				paramName = value;
			} else if (state == 31) {
				if (paramName == null) {
					System.err.println("Context parameter value '" + value + "' without name");
					System.exit(-1);
				}
				contextParams.put(paramName, value);//paramName : value for servletContext
				paramName = null;
				state = 0;
			} else if (state == 41) {
				if (paramName == null) {
					System.err.println("Servlet parameter value '" + value + "' without name");
					System.exit(-1);
				}
				HashMap<String,String> servletParam = servletParams.get(servletName);
				if (servletParam == null) {
					servletParam = new HashMap<String,String>();
					servletParams.put(servletName, servletParam);//servletName : servletConfigParams
				}
				servletParam.put(paramName, value);//paramName : value for servletConfig
				paramName = null;
				state = 0;
			}else if(state == 5){
				sessionTimeout = Integer.valueOf(value) * 60;//the set in web.xml uses minutes, so here convert it into seconds
				state = 0;
			}
		}
	}
	
	//parsing xml file according to the pattern defined in handler
	public void parseWebdotxml(String xmlPath) {
		Handler h = this.new Handler();
		File xmlFile = new File(xmlPath);
		if (!xmlFile.exists()) {
			System.err.println("error: cannot find " + xmlFile.getPath());
			System.exit(-1);
		}
		try{
			SAXParser parser = SAXParserFactory.newInstance().newSAXParser();
			parser.parse(xmlFile, h);
		}catch(SAXException | ParserConfigurationException | IOException ex){
			ex.printStackTrace();
		}
	}
	
	//get the set of sessionTimeout in web.xml
	public int getSessionTimeoutSet(){
		return sessionTimeout;
	}
	
	//fill "context-param" field in web.xml into servletContext
	public void fillContext() {
		for (String param : contextParams.keySet()) servletContext.setInitParam(param, contextParams.get(param));
	}
	
	//create a servletConfig(if exists) for each servlet
	public void createConfigs() {
		for (String servletName : servletParams.keySet()) {
			HttpServletConfig servletConfig = new HttpServletConfig(servletName, servletContext);
			HashMap<String,String> servletParam = servletParams.get(servletName);
			if (servletParam != null) {
				for (String param : servletParam.keySet()) {
					servletConfig.setInitParam(param, servletParam.get(param));
				}
				servletConfigs.put(servletName, servletConfig);
			}
		}
	}
	
	//the main parsing process
	public void parse(){
		parseWebdotxml(xmlPath);
		fillContext();
		createConfigs();
	}
	
	/*
	public static void main(String[] args) throws Exception {
		
		Handler h = parseWebdotxml(args[0]);
		HttpServletContext context = createContext(h);
		
		HashMap<String,HttpServlet> servlets = createServlets(h, context);
		
		Session fs = null;
		
		for (int i = 1; i < args.length - 1; i += 2) {
			HttpRequest request = new HttpRequest(fs);
			HttpResponse response = new HttpResponse();
			String[] strings = args[i+1].split("\\?|&|=");
			HttpServlet servlet = servlets.get(strings[0]);
			if (servlet == null) {
				System.err.println("error: cannot find mapping for servlet " + strings[0]);
				System.exit(-1);
			}
			for (int j = 1; j < strings.length - 1; j += 2) {
				request.setParameter(strings[j], strings[j+1]);
			}
			if (args[i].compareTo("GET") == 0 || args[i].compareTo("POST") == 0) {
				request.setMethod(args[i]);
				servlet.service(request, response);
			} else {
				System.err.println("error: expecting 'GET' or 'POST', not '" + args[i] + "'");
				usage();
				System.exit(-1);
			}
			
			fs = (Session) request.getSession(false);
			
		}
	}
	*/
}
 
