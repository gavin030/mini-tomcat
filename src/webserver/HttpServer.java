package webserver;

import webserver.servlet.HttpServletConfig;
import webserver.servlet.HttpServletContext;

import java.lang.Long;
import java.util.HashMap;
import sun.management.ConnectorAddressLink;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.PropertyConfigurator;
import org.apache.log4j.Logger;

public class HttpServer {
	public static final HashMap<String,String> servletClassNames = new HashMap<String,String>();//servletName : servletClassName
	public static final HashMap<String, String> urlPatterns = new HashMap<>();//servletName : urlPattern
	public static final HashMap<String, String> servletNames = new HashMap<>();//urlPattern : servletName
	public static final HashMap<String, HttpServletConfig> servletConfigs = new HashMap<>();//servletName : servletConfig
	public static final HttpServletContext servletContext = new HttpServletContext();
	public static final HashMap<String, HttpSession> sessions = new HashMap<>();
	public static int sessionTimeout;
	
	public static void main(String[] args){
		
		final int port;
		final String root;
		final String xmlPath;
		final Connector connector;    
		
		PropertyConfigurator.configure("conf/log4j.properties");
		Logger logger = Logger.getLogger("ServerLogger");
		logger.info("debug here!");
		logger.warn("ware here!");
		
		//testing command
		//java -cp target/WEB-INF/lib/webserver.jar:target/WEB-INF/lib/servlet-api.jar edu.upenn.cis455.webserver.HttpServer 8080 /home/cis455/workspace/HW1
		//stress test: ab -n 10000 -c 1000 http://127.0.0.1:8082/
		if(args.length == 3){
			port = 80;
			root = null;
			connector = null;
			System.err.println("usage: java HttpServer <port> <root path> <path to web.xml>");
		}else{
			port = 8082;//Integer.parseInt(args[0]);//
			root = "/home/cis455/Bitbucket/Mini-Tomcat/";//args[1];//
			xmlPath = "src/webapps/WebApplicationDemo1/web/WEB-INF/web.xml";//args[2];//
			final WebxmlParser xmlParser = new WebxmlParser(xmlPath, servletClassNames, urlPatterns, servletConfigs, servletContext);
			xmlParser.parse();
			sessionTimeout = xmlParser.getSessionTimeoutSet();
			for(String servletName : urlPatterns.keySet()) servletNames.put(urlPatterns.get(servletName), servletName);
			
			//testing the xmlParser
			for(String servletName : servletClassNames.keySet())
				System.out.print(servletName + ":" + servletClassNames.get(servletName) + " ");
			System.out.println();
			for(String servletName : urlPatterns.keySet())
				System.out.print(servletName + ":" + urlPatterns.get(servletName) + " ");
			System.out.println();
			for(String servletName : servletConfigs.keySet()){
				System.out.print(servletName + ":" + servletConfigs.get(servletName) + " ");
			}
			System.out.println();
			System.out.println(servletContext.getInitParameter("webmaster"));
			System.out.println("sessionTimeout: " + sessionTimeout);
			
			connector = new Connector(port, root);
			connector.setLogger(logger);
			connector.listen();
		}
	}
}